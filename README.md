# Bad-Ass Baby Monitor

When I was on the market for my first baby-monitor I quickly realized the "baby industry" is just
like the "wedding industry."  Everything was overpriced just because it had the word "_baby_" in the 
name.  Baby monitors were just cameras.  There is no reason to jack up the price because it has the 
words "_Baby Monitor_" on the box.  I decided to create my own, on a budget, with **features I 
wanted**. 

## Current Features

- Web-based video and audio monitoring.

## Planned Features (this project is a work-in-progress!)

- Various "alerting" or notification schemes that make sense across the plethora of client devices.
    - Android, iOS, MacOS, and Linux.
    - Chrome, and Safari.
- Night Vision, with programmatic on/off and luminosity strength capabilities.
- Motion detection, with configurable alerts or notifications.
- Room temperature monitoring.

# Build Your Own

I built the baby-monitor for about $100 (USD).  I bought all the hardware on Amazon, which I listed 
in [HARDWARE.md](https://gitlab.com/rbprogrammer/baby-monitor/blob/master/docs/HARDWARE.md).

The software is a combination of code I found on GitLab and code that I cobbled together.

# TODO

Since this project is still a work in progress (WIP):

- [ ] Add a logger.
- [ ] Make sure each setup script has a description saying what it does, the context it's expected to run in, and why it does what it does.
- [ ] Make sure each setup script is idempotent, or at least explicitly say why it is not.
- [ ] Put [yamljs](https://github.com/jeremyfa/yaml.js) interpolation capability into yamljs, or at least create a pull-request for it?
- [ ] Add [motion detection](http://codersblock.com/blog/motion-detection-with-javascript/)
- [ ] Add [audio](https://www.npmjs.com/package/play-sound) for white noise and/or music.

