# System Updates

There were a few changes that I made to the Raspberry Pi that I wanted to keep
track of, even though none of them were directly related to the baby-monitor 
itself.

## Enable SSH

If SSH is not already enabled, you'll need to connect the Raspberry Pi up to a
monitor and log in locally.

Open the file `/etc/ssh/sshd_config` and enable: `PasswordAuthentication yes`.

Now turn on the SSH daemon.

```bash
sudo systemctl enable ssh
sudo systemctl start ssh
``` 

## Disable WiFi and Bluetooth - Optional

Since I do not plan on using WiFi nor Bluetooth, I chose to disable those 
on-board adapters for my RPi.  I did this by modifying the boot config file.

1. `sudo vim /boot/config.txt`
1. Near the end of the file, find the section that starts with `# Additional overlays and ...`.
1. Add these two lines:
  - `dtoverlay=pi3-disable-bt`
  - `dtoverlay=pi3-disable-wifi`
1. `sudo reboot`

You can confirm the WiFi adapter was disabled with `ifconfig -a`.

[dtoverlay documentation](https://github.com/raspberrypi/firmware/blob/master/boot/overlays/README)

## Apt The World

```bash
sudo apt update -y
sudo apt upgrade -y
sudo apt-get -y dist-upgrade
sudo apt-get autoremove

declare -a REQUIRED=(
  docker.io
  libharfbuzz0b
  libfontconfig1
  vim
)
sudo apt install -y ${REQUIRED[*]}

curl -sL https://deb.nodesource.com/setup_9.x | sudo -E bash -
declare -a OPTIONALS=(
  build-essential
  git
  nginx
  nodejs
)
sudo apt install -y ${OPTIONALS[*]}
```

## Enable the Pi Camera

1. `sudo raspi-config`
1. Select the _Interfacing Options_ option.
1. Select the _Camera_ option.
1. Choose _Yes_ when it asks to enable the camera interface.
1. Choose the _Finish_ option.

Also make sure there is at least 128MB of GPU memory for the camera!

## Disable Password Authentication

Now that we have SSH keys to log into the Raspberry Pi it's best to disable
password authentication through SSH.  But first we must create an SSH key so we
can still log into the RPi from your development computer:

```bash
# If you do not already have an SSH key, generate one:
ssh-keygen -b 2048

# Copy the key to the Raspberry Pi. 
ssh-copy-id ${NEW_USER}@babymonitor
```

Next, open the file `/etc/ssh/sshd_config` and disable the property:
`PasswordAuthentication no`.

Now restart the SSH daemon.

```bash
sudo systemctl restart ssh
``` 

