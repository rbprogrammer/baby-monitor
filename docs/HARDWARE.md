# Hardware

This baby-monitor will most likely work on almost any Rasyberry Pi platform, but for completeness 
this is the hardware that I tested and now use this software on:

- [CanaKit Raspberry Pi 3 Kit with Clear Case and 2.5A Power Supply](https://www.amazon.com/gp/product/B01C6EQNNK/ref=oh_aui_detailpage_o00_s00?ie=UTF8&psc=1)

- [Kuman for Raspberry PI Camera Module 5MP 1080p OV5647 Sensor HD Video Webcam Supports Night Vision](https://www.amazon.com/gp/product/B0759GYR51/ref=oh_aui_detailpage_o00_s01?ie=UTF8&psc=1)

- [Samsung 32GB 95MB/s (U1) MicroSD EVO Select Memory Card with Adapter](https://www.amazon.com/gp/product/B06XWN9Q99/ref=oh_aui_detailpage_o00_s00?ie=UTF8&psc=1)

- [SunFounder USB 2.0 Mini Microphone](https://www.amazon.com/gp/product/B01KLRBHGM/ref=oh_aui_detailpage_o00_s00?ie=UTF8&psc=1)

- [DS18B20 Temperature Sensor](https://www.amazon.com/SUNKEE-DS18B20-Temperature-Sensor-DS18B20DS18B20/dp/B008HODWBU/ref=sr_1_16?ie=UTF8&qid=1524340238&sr=8-16&keywords=DS18B20)

At the time I purchased this equipment it cost a grand total of: $101.91 (USD).

# Custom Modifications

The Kuman Raspberry Pi camera I bought did not fit on the inside of the CanaKit case.  I needed to
use a Dremel tool to bore out the Raspberry Pi logo on the front of the case.  Then the Kuman Pi 
camera could be glued to the camera prongs on the inside of the case.  (I said I wanted a 
baby-monitor with bad-ass features, I never said I wanted it to look pretty.)

Additionally, I did not want the IR LEDs constantly shining.  I never attached them to the Kuman
camera, but rather used a small Dremel bit to drill tiny holes near the base of the case.  I then
attached the leads to the IR LEDs to some GPIO pins for programmatic control.

# Schematic Parts

Additionally, I had a few electronic parts that I did not include into my budget. Things like wires,
resistors, transistors, etc.  You can get everything needed to build this project with these parts:

- [Elegoo 120pcs Multicolored Dupont Breadboard Jumper Wires Ribbon Cables Kit](https://www.amazon.com/Elegoo-EL-CP-004-Multicolored-Breadboard-arduino/dp/B01EV70C78/ref=sr_1_1?ie=UTF8&qid=1523105115&sr=8-1&keywords=Elegoo+120pcs+Multicolored+Dupont+Wire+40pin+Male+to+Female%2C+40pin+Male+to+Male%2C+40pin+Female+to+Female+Breadboard+Jumper+Wires+Ribbon+Cables+Kit)

- [Transistor Kit 10 Value 200pcs NPN PNP Power Transistor Assortment Kit](https://www.amazon.com/Transistor-200pcs-Assortment-Transistors-Kulannder/dp/B077W8FFCC/ref=sr_1_1?ie=UTF8&qid=1524340153&sr=8-1&keywords=NPN+PNP+Transistor+TO-92+Power+Transistor+Assortment+Kit+2N2222+2N3904)


