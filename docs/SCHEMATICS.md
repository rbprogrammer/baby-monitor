# Schematics

[GPIO pin layout](https://www.w3schools.com/nodejs/nodejs_raspberrypi_gpio_intro.asp)

Note: GPIO pins are +3.3v when turned on, and +0v when off.

# Night Vision

```
        +--------\/\/\/\/--------o (Pin 1)
        |          (R1)
        |
      __|__
      \ ~ /
      _\_/_  (LED)
        |
        |
        |
        \|         (R2)
   (T1)  |-------\/\/\/\/--------o (Pin XYZ)
        /|
        |
        |
        |
        +------------------------o (Pin 6)
```

[Control LED Using GPIO Output Pin](https://www.raspberrypi-spy.co.uk/2012/06/control-led-using-gpio-output-pin/)

## Components

R1 = 0-Ω

R2 = 120-Ω

T1 = [BC337](https://www.onsemi.com/pub/Collateral/BC337-D.PDF)

LED = Repurposed from the IR LEDs that came with my Pi Camera module.  See 
[HARDWARE.md](https://gitlab.com/rbprogrammer/baby-monitor/blob/master/docs/HARDWARE.md).

## Pins

Pin 1 = +3.3v

Pin 6 = +0v (GND)

Pin XYZ = Ping 16 and 18 = GPIO 23 and 24

## Description

Technically, R1 is needed to help reduce the voltage across the LED.  However, since I connected to
the +3.3v power pin, and the voltage and current needed for my specific LED, I decided not use to
a resistor in series before the LED.  If you use the +5v power supply and/or use a different set of
IR LEDs than me it might be wise to check the datasheet for your LED and calculate whether or not
you'll need a resistor for R1.  Also, since the base pin on the transistor is connected to a GPIO
pin (through R2), the GPIO pins provide +3.3v when turned on.  This also played a part in why I
chose to use the +3.3v power supply and R1=0.

I decided to use GPIO pins 23 and 24.  They were completely arbitrary, and you can choose to use 
other pins.  However, make sure they match the configuration file
[nodejs.yml](https://gitlab.com/rbprogrammer/baby-monitor/blob/master/config/nodejs.yml).  I used
GPIO pins because I have two IR LEDs and wanted to control them independently.  Basically I
duplicated the above circuit twice.

Finally, I used BC337 transistors.  Again, almost a completely arbitrary decision.  They are what I
had on hand which required me to tweak the resistor values from similar circuits found online.  If 
you use different transistors, do your homework.  Find the data sheets.  Figure out what resistor 
values will make the circuit operate the way you expect. 

# Thermometer

```
        +-------+----------------o (Pin 1)
        |       |
        |       \
        |       / (R1)
        |       \
        |       /
        |       |
        \|      |  
  (Th1)  |------+----------------o (Pin 7)
        /|
        |
        |
        |
        +------------------------o (Pin 6)
```

[Raspberry Pi Thermometer](https://www.hackster.io/timfernando/a-raspberry-pi-thermometer-you-can-access-anywhere-33061c)

[Sensors - Temperature with the 1-Wire interface and the DS18B20](https://thepihut.com/blogs/raspberry-pi-tutorials/18095732-sensors-temperature-with-the-1-wire-interface-and-the-ds18b20)

## Components

R1 = 4.7-kΩ

Th1 = [DS18B20](https://datasheets.maximintegrated.com/en/ds/DS18B20.pdf)

## Pins

Pin 1 = +3.3v

Pin 6 = +0v (GND)

Pin 7 = GPIO 4

## Description

GPIO 4 is the default pin for the [1-Wire Interface](https://pinout.xyz/pinout/1_wire).  You can
change which pin you want to use with the `dtoverlay` command.  The `dtoverlay` command is ran
dynamically before the baby monitor is started from the 
[baby-monitor.service](https://gitlab.com/rbprogrammer/baby-monitor/blob/master/config/baby-monitor.service)
file.  Optionally, you can add the `dtoverlay` command to `/boot/config.txt`.

Since the DS18B20 implements a standard 1-wire protocol and uses +3.3v power, a 4.7-kΩ pull-up
resistor is typical.
