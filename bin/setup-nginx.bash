#!/usr/bin/env bash
# ==============================================================================
# This script will idempotenly set up test TLS certificates for the baby-
# monitor.  If certificates already exist then this script will not build the
# certificate for the root CA cert, signing CA cert, then finally the TLS cert.
#
# Much of the logic in here, and the associated "openssl-*.ini" config files
# have been adapted from:
#   http://pki-tutorial.readthedocs.io/en/latest/simple/
#
# This script and the related "openssl-*.ini" config files desperately needs to
# use configurable filenames from "bash.env".  This will make the code cleaner
# and reduce the need to duplicate names into "nodejs.yml".
# ------------------------------------------------------------------------------

# Do not allow empty variables.
set -o nounset

HEADER_COUNT=0
function print_header() {
    let HEADER_COUNT++
    echo
    echo '----------------------------------------'
    echo "${HEADER_COUNT}) ${@}"
    echo
}

# Interpolates bash environment variables in the runtime config file $1.
function interpolate_config() {
    local FILE="${1}"
    local TEMP_NAME="$(mktemp --dry-run /tmp/baby-monitor-XXXXXXXX)"
    local TEMP_SCRIPT="${TEMP_NAME}.original"

    # Create a new script that will cat the original file.
    touch "${TEMP_SCRIPT}"
    echo 'cat <<END_OF_TEXT' >"${TEMP_SCRIPT}"
    cat "${FILE}"            >>"${TEMP_SCRIPT}"
    echo 'END_OF_TEXT'       >>"${TEMP_SCRIPT}"

    # Use Bash to interpolate the original file into a new file.
    local INTERPOLATED_FILE="${TEMP_NAME}.interpolated"
    bash <"${TEMP_SCRIPT}" >"${INTERPOLATED_FILE}"
    sudo mv -fv "${INTERPOLATED_FILE}" "${FILE}"

    # Delete the temp file.
    rm -fv "${TEMP_SCRIPT}"
}

# ==============================================================================
print_header 'Initializing script configuration.'
PROJECT_DIR="$(cd "$(readlink -f $(dirname ${BASH_SOURCE[0]}))/../" ; pwd)"
cd "${PROJECT_DIR}"

source "${PROJECT_DIR}/config/bash.env"

# ==============================================================================
print_header 'Installing nginx site.'
PROJECT_SITE_FILENAME=baby-monitor.nginx
SYSTEM_SITE_AVAILABLE=/etc/nginx/sites-available/${PROJECT_SITE_FILENAME}
sudo cp -fv "./config/${PROJECT_SITE_FILENAME}" "${SYSTEM_SITE_AVAILABLE}"
interpolate_config "${SYSTEM_SITE_AVAILABLE}"
sudo chmod -c 644 "${SYSTEM_SITE_AVAILABLE}"
sudo chown -c root:root "${SYSTEM_SITE_AVAILABLE}"

# ==============================================================================
print_header 'Enabling nginx site.'
SYSTEM_SITE_ENABLED=/etc/nginx/sites-enabled/${PROJECT_SITE_FILENAME}
sudo ln -sfv "${SYSTEM_SITE_AVAILABLE}" "${SYSTEM_SITE_ENABLED}"

# ==============================================================================
print_header 'Printing nginx site.'
ls -al "$(dirname "${SYSTEM_SITE_ENABLED}")"
echo
cat "${SYSTEM_SITE_ENABLED}"

# ==============================================================================
print_header 'Restarting nginx.'
sudo systemctl restart nginx
sudo systemctl status --full --no-pager nginx
