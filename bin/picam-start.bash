#!/usr/bin/env bash
# ==============================================================================
# Since I've decided to use picam in the fastest possible way, I've configured
# it to stream video to shared memory rather than constantly pound the SD card.
#
# This script will start picam.  In doing so, it creates the expected directory
# structure in the shared memory directory.  The shared memory directories are
# blown away after the Raspberry Pi restarts.  Thus, this recreates the
# directories every time the picam process is started.
# ------------------------------------------------------------------------------

# Do not allow empty variables.
set -o nounset

# Turn on job control.
set -o monitor

HEADER_COUNT=0
function print_header() {
    let HEADER_COUNT++
    echo
    echo '----------------------------------------'
    echo "${HEADER_COUNT}) ${@}"
    echo
}

function echoerr() {
    echo 'ERROR:' ${@} 1>&2
}

# ==============================================================================
print_header 'Asserting group access.'

HAS_GROUP="$(groups | grep audio)"
if [[ -z "${HAS_GROUP}" ]] ; then
    echoerr "Cannot run picam as: $(id)"
    echoerr 'User requires audio group access.'
    exit
fi

HAS_GROUP="$(groups | grep video)"
if [[ -z "${HAS_GROUP}" ]] ; then
    echoerr "Cannot run picam as: $(id)"
    echoerr 'User requires video group access.'
    exit
fi

# ==============================================================================
print_header 'Initializing script configuration.'
PROJECT_DIR="$(cd "$(readlink -f $(dirname ${BASH_SOURCE[0]}))/../" ; pwd)"
cd "${PROJECT_DIR}"

source "${PROJECT_DIR}/config/bash.env"

# ==============================================================================
print_header 'Finding ALSA device.'
if [[ ! -z "${PC_ALSA_DEVICE_NAME}" ]] ; then
    echo 'Using device name from config file'
else
    echo 'Determining ALSA device name.'
    DEVICE_INFO="$(arecord -l | grep card | head -n 1)"
    CARD_NUM="$(echo ${DEVICE_INFO} | grep -oP 'card \d+' | cut -d' ' -f2)"
    DEVICE_NUM="$(echo ${DEVICE_INFO} | grep -oP 'device \d+' | cut -d' ' -f2)"
    export PC_ALSA_DEVICE_NAME="hw:${CARD_NUM},${DEVICE_NUM}"
fi

if [[ -z "${PC_ALSA_DEVICE_NAME}" ]] ; then
    echoerr 'Cannot determine ALSA device name.'
    exit
else
    echo "ALSA device name: ${PC_ALSA_DEVICE_NAME}"
fi

# ==============================================================================
print_header 'Creating runtime directory structure.'
rm -rf "${PC_RUN_DIR}"
if [[ -z "${PC_SHMEM_DIR}" ]] ; then
    echo 'Using hard-disk for runtime files.'
    mkdir -pv "${PC_RUN_DIR}"
else
    echo 'Using shared-memory for runtime files.'
    mkdir -pv "${PC_SHMEM_DIR}"
    ln -fnsv "${PC_SHMEM_DIR}" "${PC_RUN_DIR}"
fi

# Make all parent "archive" directories.
PC_ARCHIVE_DIR="${PC_RUN_DIR}/rec/archive"
mkdir -pv "${PC_ARCHIVE_DIR}"

# Delete the low-level "archive" directory.
rm -rfv "${PC_ARCHIVE_DIR}"

# Ensure the low-level "archive" directory is a soft-link to a bit-bucket.
ln -sv "/dev/null" "${PC_RUN_DIR}/rec/archive"

# Make the HLS directory.
export PC_HLS_OUTPUT_DIR="${PC_RUN_DIR}/hls"
mkdir -pv "${PC_HLS_OUTPUT_DIR}"

# Set up recording hook.
mkdir -pv "${PC_RUN_DIR}/hooks"
touch "${PC_RUN_DIR}/hooks/start_record"

# ==============================================================================
print_header 'Displaying directory tree.'
ls -ald "${PC_RUN_DIR}"
tree -pu "${PC_RUN_DIR}"

# ==============================================================================
print_header 'Determining PiCam arguments.'
PC_CMD_ARGS="$(cat ${PROJECT_DIR}/config/picam.args)"

# ==============================================================================
print_header 'Starting picam.'
cd "${PC_RUN_DIR}"
export PC_CMD_ARGS="--hlsdir ${PC_HLS_OUTPUT_DIR} --alsadev ${PC_ALSA_DEVICE_NAME} ${PC_CMD_ARGS}"
echo ${PC_CMD} ${PC_CMD_ARGS}
${PC_CMD} ${PC_CMD_ARGS}
