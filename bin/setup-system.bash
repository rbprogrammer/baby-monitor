#!/usr/bin/env bash
# ==============================================================================
# This script will idempotently set up the host (i.e. Raspberry Pi) to run this
# baby monitor.  This only needs to be set up once, and depending on your
# current OS configuration you may not even need to run this script anyway.
# This script mainly documents much of the setup I needed to do on a fresh
# Raspberry Pi.
# ------------------------------------------------------------------------------

HEADER_COUNT=0
function print_header() {
    let HEADER_COUNT++
    echo
    echo '----------------------------------------'
    echo "${HEADER_COUNT}) ${@}"
    echo
}

function get_dependencies() {
    FILE=${1}
    cat "${FILE}" \
        | sed  -e 's/#.*$//' -e '/^$/d' \
        | tr '\n' ' ' \
        | tr -s ' '
}

# ==============================================================================
print_header 'Initializing script configuration.'
PROJECT_DIR="$(cd "$(readlink -f $(dirname ${BASH_SOURCE[0]}))/../" ; pwd)"
cd "${PROJECT_DIR}"

source "${PROJECT_DIR}/config/bash.env"

# ==============================================================================
print_header "Create the new user: ${BM_SERVER_USER}"
getent passwd ${BM_SERVER_USER} &>/dev/null
if [[ "$?" == "0" ]] ; then
    echo "User already exists: ${BM_SERVER_USER}"
else
    sudo useradd --create-home --shell /bin/bash ${BM_SERVER_USER}
    sudo passwd ${BM_SERVER_USER}
    sudo usermod -aG sudo ${BM_SERVER_USER}
    sudo usermod -aG audio ${PC_RUN_USER}
    sudo usermod -aG video ${PC_RUN_USER}
    sudo usermod -aG gpio ${PC_RUN_USER}
fi

# ==============================================================================
print_header "Delete the old 'pi' user."
OLD_USER=pi
getent passwd ${OLD_USER} &>/dev/null
if [[ "$?" != "0" ]] ; then
    echo "User already removed: ${OLD_USER}"
else
    OLD_HOME=$(getent passwd ${OLD_USER} | cut -d':' -f6)
    sudo userdel ${OLD_USER}
    sudo rm -rfv ${OLD_HOME}
fi

# ==============================================================================
print_header "Installing system packages."
PACKAGES="$(get_dependencies ./config/system-dependencies.txt)"
echo "Installing: ${PACKAGES}"
sudo apt install -y ${PACKAGES}

# ==============================================================================
print_header "Installing NodeJS."
which nodejs &>/dev/null
if [[ "${?}" != "0" ]] ; then
    curl -sL https://deb.nodesource.com/setup_9.x | sudo -E bash -
    sudo apt install -y nodejs
fi

# ==============================================================================
declare -a NODE_CMD=(  node  nodejs  )
print_header "Setting up capabilities for: ${NODE_CMD[@]}"
for NODE in "${NODE_CMD[@]}" ; do
    sudo setcap 'cap_net_bind_service=+ep' "$(readlink -f $(which ${NODE}))"
    sudo setcap -v 'cap_net_bind_service=+ep' "$(readlink -f $(which ${NODE}))"
done

# ==============================================================================
# End of script...
echo
