#!/usr/bin/env bash
# ==============================================================================
# Sets up the Baby Monitor software, that includes setting up picam driver,
# install systemd service files, and initialize the NodeJS packages.
# ------------------------------------------------------------------------------

HEADER_COUNT=0
function print_header() {
    let HEADER_COUNT++
    echo
    echo '----------------------------------------'
    echo "${HEADER_COUNT}) ${@}"
    echo
}

# Interpolates bash environment variables in the runtime config file $2.
function interpolate_config() {
    local FILE="${1}"
    local TEMP_NAME="$(mktemp --dry-run /tmp/baby-monitor-XXXXXXXX)"
    local TEMP_SCRIPT="${TEMP_NAME}.original"

    # Create a new script that will cat the original file.
    touch "${TEMP_SCRIPT}"
    echo 'cat <<END_OF_TEXT' >"${TEMP_SCRIPT}"
    cat "${FILE}"            >>"${TEMP_SCRIPT}"
    echo 'END_OF_TEXT'       >>"${TEMP_SCRIPT}"

    # Use Bash to interpolate the original file into a new file.
    local INTERPOLATED_FILE="${TEMP_NAME}.interpolated"
    bash <"${TEMP_SCRIPT}" >"${INTERPOLATED_FILE}"
    sudo mv -fv "${INTERPOLATED_FILE}" "${FILE}"

    # Delete the temp file.
    rm -fv "${TEMP_SCRIPT}"
}

# ==============================================================================
print_header 'Initializing script configuration.'
PROJECT_DIR="$(cd "$(readlink -f $(dirname ${BASH_SOURCE[0]}))/../" ; pwd)"
cd "${PROJECT_DIR}"

source "${PROJECT_DIR}/config/bash.env"

# ==============================================================================
print_header 'Node initialization.'
npm prune
npm install

# ==============================================================================
print_header 'Installing PiCam.'
pushd external
tar --verbose \
    --wildcards \
    --strip=1 \
    --extract \
    --xz \
    --file "${PC_RELEASE_FILE_NAME}" \
    picam*/picam
popd

# ==============================================================================
print_header "Installing service files."
PROJECT_DIR="$(cd "$(readlink -f $(dirname ${BASH_SOURCE[0]}))/../" ; pwd)"
SYSTEMD_DIR=/etc/systemd/system
declare -a SERVICES=(
    picam.service
    baby-monitor.service
)
for SERVICE in ${SERVICES[@]} ; do
    SYSTEM_SERVICE="${SYSTEMD_DIR}/${SERVICE}"
    PROJECT_SERVICE="${PROJECT_DIR}/config/${SERVICE}"

    echo "Installing service: ${SERVICE}"
    sudo rm -fv "${SYSTEM_SERVICE}"
    sudo cp -v "${PROJECT_SERVICE}" "${SYSTEM_SERVICE}"

    echo "Interpolating: ${SYSTEM_SERVICE}"
    interpolate_config "${SYSTEM_SERVICE}"
    #cat "${SYSTEM_SERVICE}"
    echo

    echo "Fixing permissions: ${SERVICE}"
    sudo chmod -c 644 "${SYSTEM_SERVICE}"
    sudo chown -c root:root "${SYSTEM_SERVICE}"

    echo "Enabling service: ${SERVICE}"
    sudo systemctl daemon-reload
    sudo systemctl enable ${SERVICE}
    sudo systemctl restart ${SERVICE}
    echo
done
ls -al --group-directories-first "${SYSTEMD_DIR}"
