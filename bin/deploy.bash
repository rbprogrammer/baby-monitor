#!/usr/bin/env bash
# ==============================================================================
# Essentially this script deploys the baby monitor from the current/development
# machine to the target Raspberry Pi.  The intent is that this baby-monitor can
# be developed on a more reasonable development machine but easily deployed to
# the target.
# ------------------------------------------------------------------------------

# Do not allow empty variables.
set -o nounset

HEADER_COUNT=0
function print_header() {
    let HEADER_COUNT++
    echo
    echo '================================================================================'
    echo "${HEADER_COUNT}) ${@}"
    echo
}

function usage() {
cat <<EOF
USAGE:  $(basename ${BASH_SOURCE[0]}) [-h] [-s]
    -h      Display this help menu.
    -d      Disable setup scripts after deployment.

Change "config/bash-overrides.env" file for finer grain control over the
operation of this script.
EOF
}

# ==============================================================================
print_header 'Initializing script configuration.'
PROJECT_DIR="$(cd "$(readlink -f $(dirname ${BASH_SOURCE[0]}))/../" ; pwd)"
cd "${PROJECT_DIR}"

source "${PROJECT_DIR}/config/bash.env"

# Boolean flag to indicate if the setup scripts should be ran.
RUN_SETUP_SCRIPTS="$(true ; echo $?)"

# ==============================================================================
print_header 'Parsing script arguments.'

# Parse dem arguments.
while getopts "hd" opt; do
    case $opt in
        h)
            usage
            exit
            ;;
        d)
            RUN_SETUP_SCRIPTS="$(false ; echo $?)"
            ;;
        \?)
            echo "Invalid option: -$OPTARG" >&2
            ;;
        :)
            echo "Option -$OPTARG requires an argument." >&2
            exit 1
            ;;
    esac
done

# Build the full-qualified host name.
SERVER="${BM_SERVER_USER}@${BM_SERVER_HOST}"

# ==============================================================================
print_header 'Printing configuration.'
FORMAT='%-28s: %s\n'
printf "${FORMAT}" 'Git remote' ${BM_GIT_REPO}
printf "${FORMAT}" 'Deployment target' ${BM_SERVER_HOST}
printf "${FORMAT}" 'Deployment user' ${BM_SERVER_USER}
printf "${FORMAT}" 'Fully qualified server' ${SERVER}
printf "${FORMAT}" 'Remote deployment directory' ${BM_SERVER_DIR}
printf "${FORMAT}" 'Run all setup scripts' "${RUN_SETUP_SCRIPTS} (0=true, 1=false)"

# ==============================================================================
print_header 'Ensuring Git origin is updated.'
git push

# ==============================================================================
print_header 'Initializing baby monitor.'
ssh ${SERVER} ls ${BM_SERVER_DIR} &>/dev/null
NEED_TO_CLONE=$?
if [[ "${NEED_TO_CLONE}" == "0" ]] ; then
    echo 'Cloning is not necessary.'
else
    echo 'Cloning from Git remote.'
    ssh -t ${SERVER} git clone --recursive ${BM_GIT_REPO} ${BM_SERVER_DIR}
fi

# ==============================================================================
print_header 'Ensuring baby monitor software is up-to-date.'
ssh ${SERVER} "cd ${BM_SERVER_DIR} && git pull"

# ==============================================================================
if [[ "${RUN_SETUP_SCRIPTS}" == "0" ]] ; then
    print_header 'Running system setup script.'
    ssh -t ${SERVER} ${BM_SERVER_DIR}/bin/setup-system.bash

    print_header 'Running TLS setup script.'
    ssh -t ${SERVER} ${BM_SERVER_DIR}/bin/setup-tls.bash

    print_header 'Running baby-monitor setup script.'
    ssh -t ${SERVER} ${BM_SERVER_DIR}/bin/setup-baby-monitor.bash

    print_header 'Running nginx setup script.'
    ssh -t ${SERVER} ${BM_SERVER_DIR}/bin/setup-nginx.bash
fi
