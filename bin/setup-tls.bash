#!/usr/bin/env bash
# ==============================================================================
# This script will idempotenly set up test TLS certificates for the baby-
# monitor.  If certificates already exist then this script will not build the
# certificate for the root CA cert, signing CA cert, then finally the TLS cert.
#
# Much of the logic in here, and the associated "openssl-*.ini" config files
# have been adapted from:
#   http://pki-tutorial.readthedocs.io/en/latest/simple/
#
# This script and the related "openssl-*.ini" config files desperately needs to
# use configurable filenames from "bash.env".  This will make the code cleaner
# and reduce the need to duplicate names into "nodejs.yml".
#
# Reference links while developing this script and openssl-*.ini configs:
#   https://evilshit.wordpress.com/2013/06/19/how-to-create-your-own-pki-with-openssl/#script
#   https://www.phildev.net/ssl/opensslconf.html
# ------------------------------------------------------------------------------

# Do not allow empty variables.
set -o nounset

HEADER_COUNT=0
function print_header() {
    let HEADER_COUNT++
    echo
    echo '----------------------------------------'
    echo "${HEADER_COUNT}) ${@}"
    echo
}

function generate_passphrase() {
    FILE=${1}
    touch "${FILE}"
    echo "Creating passphrase file: ${FILE}"
    openssl rand -base64 48 | cut -c1-48 >"${FILE}"
}

# ==============================================================================
print_header 'Initializing script configuration.'
PROJECT_DIR="$(cd "$(readlink -f $(dirname ${BASH_SOURCE[0]}))/../" ; pwd)"
cd "${PROJECT_DIR}"

source "${PROJECT_DIR}/config/bash.env"

CONFIG_DIR="${PROJECT_DIR}/config"

# ==============================================================================
print_header 'Initializing PKI Directory Structure.'
mkdir -pv run/pki
cd run/pki

# ==============================================================================
print_header 'Creating Root CA.'

if [[ ! -f ca/root-ca.crt ]] ; then

    # Create directories.
    mkdir -pv ca/root-ca/private
    mkdir -pv ca/root-ca/db
    mkdir -pv crl
    mkdir -pv certs
    chmod -c 700 ca/root-ca/private

    # Create the database.
    cp -v /dev/null ca/root-ca/db/root-ca.db
    cp -v /dev/null ca/root-ca/db/root-ca.db.attr
    echo 01 > ca/root-ca/db/root-ca.crt.srl
    echo 01 > ca/root-ca/db/root-ca.crl.srl

    # Create the Root CA key passphrase.
    generate_passphrase ca/root-ca/private/root-ca.pass

    # Create the CA request.
    openssl req -new \
        -config "${CONFIG_DIR}/openssl-root-ca.ini" \
        -out ca/root-ca.csr \
        -keyout ca/root-ca/private/root-ca.key \
        -passout file:ca/root-ca/private/root-ca.pass

    # Create the CA certificate.
    openssl ca -selfsign \
        -config "${CONFIG_DIR}/openssl-root-ca.ini" \
        -in ca/root-ca.csr \
        -out ca/root-ca.crt \
        -extensions root_ca_ext \
        -passin file:ca/root-ca/private/root-ca.pass \
        <<EOF
y
y
EOF
fi

# ==============================================================================
print_header 'Creating Signing CA.'

if [[ ! -f ca/signing-ca.crt ]] ; then

    # Create directories.
    mkdir -pv ca/signing-ca/private
    mkdir -pv ca/signing-ca/db
    mkdir -pv crl
    mkdir -pv certs
    chmod -c 700 ca/signing-ca/private

    # Create the database.
    cp -v /dev/null ca/signing-ca/db/signing-ca.db
    cp -v /dev/null ca/signing-ca/db/signing-ca.db.attr
    echo 01 > ca/signing-ca/db/signing-ca.crt.srl
    echo 01 > ca/signing-ca/db/signing-ca.crl.srl

    # Create the Signing CA key passphrase.
    generate_passphrase ca/signing-ca/private/signing-ca.pass

    # Create the CA request.
    openssl req -new \
        -config "${CONFIG_DIR}/openssl-signing-ca.ini" \
        -out ca/signing-ca.csr \
        -keyout ca/signing-ca/private/signing-ca.key \
        -passout file:ca/signing-ca/private/signing-ca.pass

    # Create the CA certificate/
    openssl ca \
        -config "${CONFIG_DIR}/openssl-root-ca.ini" \
        -in ca/signing-ca.csr \
        -out ca/signing-ca.crt \
        -extensions signing_ca_ext \
        -passin file:ca/root-ca/private/root-ca.pass \
        <<EOF
y
y
EOF

    # Create a simple PEM file of the CA certificate in for form that Android
    # iOS can install it as a trusted cert.
    openssl x509 -in ca/signing-ca.crt -out ca/signing-ca.crt.pem
fi

# ==============================================================================
print_header 'Creating TLS Certificate.'

if [[ ! -f certs/${BM_SERVER_HOST}.crt ]] ; then

    # Create directories.
    mkdir -pv certs/private
    chmod -c 700 certs/private

    # Create the TLS Cert key passphrase.
    generate_passphrase certs/private/${BM_SERVER_HOST}.pass

    # Create TLS server request.
    openssl req -new \
        -config "${CONFIG_DIR}/openssl-tls-cert.ini" \
        -out certs/${BM_SERVER_HOST}.csr \
        -keyout certs/private/${BM_SERVER_HOST}.key \
        -passout file:certs/private/${BM_SERVER_HOST}.pass

    pwd

    # Create TLS server certificate.
    openssl ca \
        -config "${CONFIG_DIR}/openssl-signing-ca.ini" \
        -in certs/${BM_SERVER_HOST}.csr \
        -out certs/${BM_SERVER_HOST}.crt \
        -extensions server_ext \
        -passin file:ca/signing-ca/private/signing-ca.pass \
        <<EOF
y
y
EOF
fi

# ==============================================================================
print_header 'Ensuring Proper Permissions.'

find . -wholename '*.key' | xargs chmod -c 400
find . -wholename '*.pass' | xargs chmod -c 400
