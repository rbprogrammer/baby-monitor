// =================================================================================================
// This module encapsulates the logic to read in the configuration file.  Also encapsulating this
// into a module makes the retrieval process for properties faster since the module is loaded once.
// The properties are then stored in memory rather than reaching to disk every time a property is
// needed.
// =================================================================================================

/**
 * Returns either the value of the supplied environment variable, or the original regex match.
 *
 * @param originalMatch The original match that caused this replacement function to get invoked.
 * @param envVariable The environment variable name to interpolate with.
 *
 * @returns {string} Either the environment variable or the original matched substring.
 */
function find_env_value(originalMatch, envVariable) {
    const envValue = process.env[envVariable];
    return !!envValue ? envValue : originalMatch;
}

/**
 * Interpolate the supplied string with environment variables.
 *
 * @param strValue The string to interpolate variables within.
 *
 * @returns {string} A new string with the interpolated results.
 */
function interpolate_string(strValue) {
    return strValue.replace(/\${ENV:([A-Za-z0-9_]+)}/g, find_env_value);
}

/**
 * Recursively iterates over the supplied object interpolating.
 *
 * For objects, this method will recursively call itself on the sub-object.
 *
 * For strings, this method will perform a global string replacement for anything that matches the
 * format "${env:VARNAME}" with the value of the environment variable VARNAME.
 *
 * For booleans and numbers, this method simply leaves the value as it is.
 *
 * For any other object type this method will simply log an error message indicating it does not
 * know how to handle interpolation for that object type.
 *
 * @param obj The object to recursively interpolate strings.
 *
 * @returns {object} The supplied object with interpolated strings.
 */
function interpolate_object(obj) {
    for (var property in obj) {
        if (obj.hasOwnProperty(property)) {
            switch (typeof(obj[property])) {

                case 'object':
                    interpolate_object(obj[property]);
                    break;

                case 'string':
                    obj[property] = interpolate_string(obj[property]);
                    break;

                case 'boolean':
                case 'number':
                    // No-op... just keep the original value.
                    break;

                default:
                    console.error('Cannot interpolate unknown object type: '
                        + typeof(obj[property])
                        + ': '
                        + obj[property]);
                    break;
            }
        }
    }
    return obj;
}

// Export the YAML configuration as top-level fields.
const yaml = require('yamljs');
const config_obj = yaml.load('./config/nodejs.yml');
module.exports = interpolate_object(config_obj);
