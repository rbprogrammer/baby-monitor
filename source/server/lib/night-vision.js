const Gpio = require('onoff').Gpio;

const arrayutils = require('./arrayutils');
const config = require('./config');

const LED_ON = 1;
const LED_OFF = 0;

// todo docs
function _getPinNumbers() {
    return config.peripherals.night_vision
        .split(',')
        .map((s) => {
            return parseInt(s.trim());
        });
}

// TODO docs - note that each LED will need to have its unexport() method called when done.
function _getAllGpioPins() {
    return _getPinNumbers().map((num) => {
        return new Gpio(num, 'out');
    });
}

// todo docs - one-based!
module.exports.getMaxIntensity = function () {
    return _getPinNumbers().length;
};

// TODO docs
module.exports.turnOn = function (intensity) {
    const allPins = arrayutils.shuffle(_getAllGpioPins());

    // Normalize the intensity so it is within [1, MaxIntensity]
    intensity = Math.min(intensity, this.getMaxIntensity());
    intensity = Math.max(intensity, 1);

    console.log('Turning on night vision with intensity: ', intensity);
    for (let i = 0; i < intensity; i++) {
        console.log(`Turning on GPIO ${allPins[i]._gpio}`);
        allPins[i].write(LED_ON, (err) => {});
    }

    allPins.forEach((pin) => {
        // TODO why can I not unexport() the objects here?
        // pin.unexport();
    });
};

// TODO docs
module.exports.turnOff = function () {
    console.log('Turning off night vision.');
    _getAllGpioPins().forEach((pin) => {
        pin.write(LED_OFF, (err) => {
            pin.unexport();
        });
    });
};

