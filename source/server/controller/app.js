// =================================================================================================
// This is the meat of the baby-monitor control as it sets up the full Node.js stack including all
// of the Express routes.
// -------------------------------------------------------------------------------------------------

const bodyParser = require('body-parser');
const config = require('../lib/config');
const express = require('express');
const helmet = require('helmet');
const morgan = require('morgan');
const nightvision = require('../lib/night-vision');
const serveFavicon = require('serve-favicon');
const util = require('util');

const app = express();
const debugMode = !!config.development.debug;

// Setup the view engine.
app.set('views', 'source/server/views');
app.set('view engine', 'pug');

// Set up all the middleware.
app.use(helmet());
app.use(serveFavicon('source/client/images/pacifier.png'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(morgan(':date[iso] :remote-addr HTTP/:http-version :method :status :response-time ms :url'));

// Set up the public routes.
app.use('/bootstrap', express.static('node_modules/bootstrap/dist'));
app.use('/hls.js', express.static('node_modules/hls.js/dist'));
app.use('/jquery', express.static('node_modules/jquery/dist'));
app.use('/popper.js', express.static('node_modules/popper.js/dist'));
app.use('/video.js', express.static('node_modules/video.js/dist'));

app.use('/images', express.static('source/client/images'));
app.use('/javascripts', express.static('source/client/javascripts'));
app.use('/stylesheets', express.static('source/client/stylesheets', {
    cache: !debugMode,
    compress: !debugMode,
    debug: debugMode
}));

// Set up the more dynamic routes.
const videoSrc = util.format("https://%s:%d/hls/index.m3u8",
                             config.server.video.interface,
                             config.server.video.port);
app.get('/', function (req, res, next) {
    res.render('index', {
        title: config.application.name,
        videoSrc: videoSrc,
        maxIntensity: nightvision.getMaxIntensity()
    });
});
app.get('/download/ca', function (req, res, next) {
    var file = config.security.ca_cert_file_pem;
    res.setHeader('Content-Type', 'application/x-x509-ca-cert');
    res.download(file);
});
app.get('/temperature', function (req, res, next) {
    res.setHeader('Content-Type', 'application/json');
    res.write(JSON.stringify({
        // TODO use the device to read the temperature.
        celsius: 0,
        fahrenheit: 0,
    }));
    res.end()
});

app.post('/night-vision', function (req, res, next) {
    // TODO validate the duration!!
    const duration = parseInt(req.body.duration);
    const intensity = parseInt(req.body.intensity);

    console.log(`Setting timer to turn off night vision after: ${duration}ms`);
    nightvision.turnOn(intensity);
    setTimeout(nightvision.turnOff, duration);

    res.end();
});

// Catch 404 and forward to error handler.
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// Generic error handler.
app.use(function (err, req, res, next) {
    // Set locals, only providing error in development.
    res.locals.message = err.message;
    res.locals.error = debugMode ? err : {};

    // Render the error page.
    res.status(err.status || 500);
    res.render('error');
});

// Finally export the top level app object.
module.exports = app;
