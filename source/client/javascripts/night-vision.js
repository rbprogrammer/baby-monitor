const slider = document.getElementById("nvRange");
const output = document.getElementById("nvRangeDisplay");
output.innerHTML = 'Intensity (' + slider.value + ')';
slider.oninput = function() {
    output.innerHTML = 'Intensity (' + this.value + ')';
};

function turnOnNightVision(duration) {
    const intensity = document.getElementById('nvRange').value;
    fetch('/night-vision', {
        method: 'POST',
        headers: {
            "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify({
            duration: duration,
            intensity: intensity
        })
    });
}
