const bmVideoIdName = 'bmVideo';
const bmVideo = document.getElementById(bmVideoIdName);
const source = bmVideo.getAttribute('data-source');
if (Hls.isSupported()) {
    var hls = new Hls();
    hls.loadSource(source);
    hls.attachMedia(bmVideo);
    hls.on(Hls.Events.MANIFEST_PARSED, function () {
        bmVideo.play();
    });
} else if (bmVideo.canPlayType('application/vnd.apple.mpegurl')) {
    bmVideo.src = source;
    bmVideo.addEventListener('canplay', function () {
        bmVideo.play();
    });
} else {
    alert('Video just not supported?  What the hell?');
}

videojs(bmVideoIdName, {
    controls: true,
    autoplay: 'false',
    preload: 'false',
    controlBar: {
        muteToggle: false,
        playToggle: false,
        progressControl: false,
        remainingTimeDisplay: false
    }
});
